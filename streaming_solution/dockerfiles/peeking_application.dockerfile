FROM python:3.8
RUN apt-get -y update && \
apt-get install -y python3-dev && \
mkdir /app
COPY /peeker /app
WORKDIR /app
RUN pip3 install -r requirements.txt
