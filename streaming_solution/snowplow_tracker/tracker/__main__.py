import logging
from string import ascii_lowercase
from time import sleep
import random, \
sys, \
signal
from typing import Optional
from snowplow_tracker import Emitter, \
Tracker


PAGEVIEW = "pageview"
LINK_CLICK = "link_click"
SEARCH_TERMS = "search_terms"

class FireEvents: 
    """ Fires random snowplow tracker
        events at the collector.
    """

    def __init__(self, 
                 wait: Optional[int] = 0.5):
        collector_url = "challenge-collector:8081"
        self.logger = logging.getLogger()
        self.logger.setLevel = logging.DEBUG
        self.logger.addHandler(logging.StreamHandler())

        self.wait = wait
        self.tracker = Tracker(Emitter(collector_url))
        #  make sure the kill command can get through
        shutdown = (lambda : sys.exit(0))
        signal.signal(signal.SIGINT, 
                      shutdown)
        self.check_broker_is_ready(collector_url)
        self.emulate_traffic()

    def emulate_traffic(self):
        self.logger.info("Begin generating events...")
        while True:
            try:
                self.emit_event()
                sleep(self.wait)
            except KeyboardInterrupt:
                self.logger.info("Keyboard exit.")
                sys.exit(0) 

    def check_broker_is_ready(self,
                              collector_url: str):
        """ Waits for the broker to be live."""
        def broker_is_ready():
            checker = kafka.KafkaConsumer(group_id='kafka_is_ready', 
                                          bootstrap_servers=collector_url)
            if not checker.topics():
                self.logger.info("Waiting for broker...")
                sleep(0.5)
                broker_is_cready()
            self.logger.info("Broker is live!")
            return True 

    @staticmethod
    def _random_word():
        return ''.join([random.choice(ascii_lowercase) 
                        for _ in range(random.randrange(5,20))])


    def emit_event(self):
        self.logger.info('Emitting an event...')
        record = self.generate_record()
        try:
            if record[0] == PAGEVIEW:
                self.tracker.track_page_view(record[1])
                self.logger.info("Emitted a page view!")
            elif record[0] == LINK_CLICK:
                self.tracker.track_link_click(record[1])
                self.logger.info("Emitted a link click!")
            else:
                self.tracker.track_site_search(record[1])
                self.logger.info("Emitted search items!")
        except Exception as e:
            print(e)  # let it die a horrible death during build
            
    def generate_record(self):
        if (record_type := random.choice([PAGEVIEW,
                                      LINK_CLICK,
                                      SEARCH_TERMS])) != SEARCH_TERMS:
            value = f"https://health-union.com/{self._random_word()}"
        else:
            value = [self._random_word() for _ in range(random.randrange(3,5))]
        return record_type, value

if __name__ == "__main__":
    fire_events = FireEvents()