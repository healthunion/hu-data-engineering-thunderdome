from kafka import KafkaConsumer
from decode import decode_msg
import sys

def main():
    print("starting peeker...")
    consumer_params = dict(bootstrap_servers="challenge-kafka:9092",
                           group_id="peeking_client")
    good_consumer = KafkaConsumer('web_events', 
                                  **consumer_params)
    bad_consumer = KafkaConsumer('bad_web_events', 
                                 **consumer_params)
                                 
    for consumer in (good_consumer, bad_consumer,):
        for message in consumer:
            print('---'*10)
            print(message.topic,
                  message.offset,
                  message.key)
            print("value:", 
                  decode_msg(message.value))

if __name__ == "__main__":
    try:
        main()           
    except KeyboardInterrupt:
        print("\n\nEnding peeking session... k bye!\n")
        sys.exit(0)